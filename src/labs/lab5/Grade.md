# Lab 5

## Total

-/20

## Break Down

CTAStation

- Variables:                    -/2
- Constructors:                 -/1
- Accessors:                    -/2
- Mutators:                     -/2
- toString:                     -/1
- equals:                       -/2

CTAStopApp

- Reads in data:                -/2
- Loops to display menu:        -/2
- Displays station names:       -/1
- Displays stations by access:  -/2
- Displays nearest station:     -/2
- Exits                         -/1

## Comments
