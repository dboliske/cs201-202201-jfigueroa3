# Lab 6

## Total

-/20

## Break Down

Deli Queue Application

- Loops to display options          -/2
- Adds customer to queue            -/6
- Removes customer from queue       -/6
- Use ArrayLists to store customers -/6

## Comments
