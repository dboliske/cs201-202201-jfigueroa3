# Lab 1

## Total

-/20

## Break Down

* Exercise 1    -/2
* Exercise 2    -/2
* Exercise 3    -/2
* Exercise 4
  * Program     -/2
  * Test Plan   -/1
* Exercise 5
  * Program     -/2
  * Test Plan   -/1
* Exercise 6
  * Program     -/2
  * Test Plan   -/1
* Documentation -/5

## Comments
