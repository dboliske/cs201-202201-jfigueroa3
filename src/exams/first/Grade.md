# Midterm Exam

## Total

-/100

## Break Down

1. Data Types:                  -/20
    - Compiles:                 -/5
    - Input:                    -/5
    - Data Types:               -/5
    - Results:                  -/5
2. Selection:                   -/20
    - Compiles:                 -/5
    - Selection Structure:      -/10
    - Results:                  -/5
3. Repetition:                  -/20
    - Compiles:                 -/5
    - Repetition Structure:     -/5
    - Returns:                  -/5
    - Formatting:               -/5
4. Arrays:                      -/20
    - Compiles:                 -/5
    - Array:                    -/5
    - Exit:                     -/5
    - Results:                  -/5
5. Objects:                     -/20
    - Variables:                -/5
    - Constructors:             -/5
    - Accessors and Mutators:   -/5
    - toString and equals:      -/5

## Comments

1.
2.
3.
4.
5.
